import * as React from 'react';
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Badge
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { IPageControllerProps } from '../global/pageController';
import withStyles, { WithStylesProps } from './withStyles';
import { storageController } from '../global';

class HeaderView extends React.Component<
  WithStylesProps<IPageControllerProps>,
  {}
> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onOpen = () => {
    const localStore = storageController.localStore;
    localStore.dispatch(
      localStore.createMethod(m => {
        m.collapsed = !m.collapsed;
      })
    );
  };

  render() {
    const { classes } = this.props;
    const { systemStore, localStore } = storageController;

    return (
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar disableGutters={true}>
          <IconButton
            color="inherit"
            aria-label="Открыть меню"
            onClick={() => this.onOpen()}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="title"
            color="inherit"
            noWrap
            className={classes.title}
          >
            {systemStore.title}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(HeaderView);
