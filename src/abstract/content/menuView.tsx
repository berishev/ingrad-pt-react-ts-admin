import * as React from 'react';
import {
  Icon,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  Divider
} from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import BusinessIcon from '@material-ui/icons/Business';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import * as classNames from 'classnames';
import { LINQ } from 'berish-linq';
import { IPageControllerProps } from '../global/pageController';
import * as components from '../components';
import ArrayComponents from '../components/arrayComponents';
import { storageController } from '../global';
import withStyles, { WithStylesProps } from './withStyles';

interface IItemType {
  title?: React.ReactNode;
  icon?: JSX.Element;
  divider?: boolean;
  action?: () => void;
  pushURL?: string;
}

class ItemType implements IItemType {
  title: React.ReactNode = null;
  icon: JSX.Element = null;
  divider: boolean = false;
  action: () => void = null;
  pushURL: string = null;
  subitems: ItemType[] = [];

  static createItem(obj: IItemType) {
    let item = new ItemType();
    item.title = obj.title;
    item.action = obj.action;
    item.icon = obj.icon;
    item.divider = !!obj.divider;
    item.pushURL = obj.pushURL;
    return item;
  }

  createSubItem = (obj: IItemType) => {
    let item = ItemType.createItem(obj);
    this.subitems.push(item);
    return item;
  };
}

interface IMenuViewState {
  collapsed: boolean;
  collapsedItems: string[];
}

class MenuView extends React.Component<
  WithStylesProps<IPageControllerProps>,
  IMenuViewState
> {
  unlistener = storageController.localStore.subscribe(m => {
    const { collapsed } = m;
    if (this.state.collapsed != collapsed) this.setState({ collapsed });
  });
  constructor(props) {
    super(props);
    this.state = {
      collapsed: storageController.localStore.collapsed,
      collapsedItems: []
    };
  }

  componentWillUnmount() {
    if (this.unlistener) this.unlistener();
  }

  prepareItems = () => {
    let router = this.props.controller.navigator;
    const localStore = storageController.localStore;

    let project = ItemType.createItem({
      title: 'Проекты',
      pushURL: '/project/list',
      icon: <FileCopyIcon />
    });

    let team = ItemType.createItem({
      title: 'Отделы',
      pushURL: '/team/list',
      icon: <BusinessIcon />
    });

    let teamUser = ItemType.createItem({
      title: 'Команды проектов',
      pushURL: '/teamuser/list',
      icon: <EqualizerIcon />
    });

    return [project, team, teamUser];
  };

  onClick = async (item: ItemType, key: string) => {
    const { collapsed } = this.state;
    const localStore = storageController.localStore;
    let router = this.props.controller.navigator;
    let action =
      item.action ||
      (item.pushURL && (() => router.push(item.pushURL))) ||
      null;
    if (action) action();
    if (item.subitems && item.subitems.length > 0) {
      let { collapsedItems } = this.state;
      let index = collapsedItems.indexOf(key);
      if (index < 0) collapsedItems.push(key);
      else if (collapsed) collapsedItems.splice(index, 1);
      if (!collapsed)
        await localStore.dispatch(
          localStore.createMethod(m => {
            m.collapsed = true;
          })
        );
      this.setState({ collapsedItems });
    } else {
      if (collapsed)
        await localStore.dispatch(
          localStore.createMethod(m => {
            m.collapsed = false;
          })
        );
    }
  };

  renderItem = (index: number, object: ItemType, items: ItemType[]) => {
    const { classes } = this.props;
    const { collapsed } = this.state;
    let item: JSX.Element = null;
    let key = `${object.pushURL}_${index}`;
    if (object.subitems && object.subitems.length > 0 && !object.pushURL) {
      item = (
        <React.Fragment key={key}>
          <ListItem button={true} onClick={() => this.onClick(object, key)}>
            {object.icon && <ListItemIcon>{object.icon}</ListItemIcon>}
            {object.title && (
              <ListItemText
                inset
                primary={object.title}
                style={{ whiteSpace: 'normal' }}
              />
            )}
            {this.state.collapsedItems.indexOf(key) != -1 ? (
              <ExpandLess />
            ) : (
              <ExpandMore />
            )}
          </ListItem>
          <Collapse
            in={collapsed && this.state.collapsedItems.indexOf(key) != -1}
            timeout="auto"
            unmountOnExit={true}
          >
            <List
              component="div"
              className={
                this.state.collapsed
                  ? classes.drawerNestedItem
                  : classes.drawerNestedItemClose
              }
            >
              {ArrayComponents.render({
                template: this.renderItem,
                elements: object.subitems,
                args: [items]
              })}
            </List>
          </Collapse>
        </React.Fragment>
      );
    } else {
      item = (
        <ListItem
          key={key}
          button={true}
          onClick={() => this.onClick(object, key)}
        >
          {object.icon && <ListItemIcon>{object.icon}</ListItemIcon>}
          {object.title && (
            <ListItemText
              inset
              primary={object.title}
              style={{ whiteSpace: 'normal' }}
            />
          )}
        </ListItem>
      );
    }
    if (object.divider) return [item, <Divider key={`${key}_divider`} />];
    return item;
  };

  render() {
    const { classes } = this.props;
    const { collapsed } = this.state;
    let { location } = this.props.controller.navigator;
    let urlKeys = LINQ.fromArray(location.pathname.split('/')).notEmpty();
    let methodPath = urlKeys.lastOrNull();
    let controllerPathURL =
      urlKeys
        .except([methodPath])
        .toArray()
        .join('/') || methodPath;
    methodPath = methodPath && methodPath.toLowerCase();
    controllerPathURL = controllerPathURL && controllerPathURL.toLowerCase();
    let items = this.prepareItems();
    let data = LINQ.fromArray(items).select(m => {
      let url = m.pushURL || '';
      url = url.toLowerCase();
      let indexFull = url.indexOf(controllerPathURL + '/' + methodPath);
      let index = url.indexOf(controllerPathURL);
      return {
        m,
        index,
        indexFull
      };
    });
    let item = data
      .where(m => m.indexFull != -1)
      .orderBy(m => m.indexFull)
      .select(m => m.m)
      .firstOrNull();
    item =
      item ||
      data
        .where(m => m.index != -1)
        .orderBy(m => m.index)
        .select(m => m.m)
        .firstOrNull();
    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(
            classes.drawerPaper,
            !collapsed && classes.drawerPaperClose
          )
        }}
        open={collapsed}
      >
        <div className={classes.appBarSpacer} />
        <List>
          {ArrayComponents.render({
            elements: items,
            template: this.renderItem,
            args: [items]
          })}
        </List>
      </Drawer>
    );
  }
}

export default withStyles(MenuView);
