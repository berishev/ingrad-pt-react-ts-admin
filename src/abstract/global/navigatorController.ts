import { History } from 'history';
import { IContentRouteProps } from '../router/contentRoute';
import { IPageControllerProps } from './pageController';
import Portal from 'berish-react-portals';
import ModalRoute from '../router/modalRoute';

export class NavigatorController<MatchGeneric> {
  static init<MatchGeneric>(
    contentRoute: IContentRouteProps<MatchGeneric>,
    controller?: NavigatorController<MatchGeneric>
  ) {
    if (!controller) controller = new NavigatorController<MatchGeneric>();
    return controller.receive(contentRoute);
  }

  private _history: History = null;
  private _params: MatchGeneric = null;

  receive(contentRoute: IContentRouteProps<MatchGeneric>) {
    this._history = contentRoute.history;
    this._params = contentRoute.params;
    return this;
  }

  get params() {
    return this._params || ({} as MatchGeneric);
  }

  get location() {
    return this._history.location;
  }

  private prepareUrl(path: string, params?: { [key: string]: any }) {
    let url = Object.keys(params || {})
      // .filter(key => params[key] != null)
      .map(key => `${key}=${params[key]}`)
      .join('&');
    return `${path}${url ? '?' + url : ''}`;
  }

  push(path: string, params?: { [key: string]: any }) {
    let paramsUrl = this.prepareUrl(path, params);
    this._history.push(paramsUrl);
  }

  pushModal<MatchGeneric, ResolveGeneric>(
    component: React.ComponentClass<
      IPageControllerProps<MatchGeneric, ResolveGeneric>
    >
  ) {
    let decorate = Portal.create(ModalRoute);
    return (params?: MatchGeneric) => {
      let props: IContentRouteProps<MatchGeneric> = {
        component,
        params,
        history: this._history
      };
      return decorate(props) as Promise<ResolveGeneric>;
    };
  }

  goBack(path: string, params?: { [key: string]: any }) {
    return this.push(path, params);
  }

  replace(path: string, params?: { [key: string]: any }) {
    let paramsUrl = this.prepareUrl(path, params);
    this._history.replace(paramsUrl);
  }
}
