import { MethodStore } from '../redux-helper';
import { userType, roleType } from '../../util/auth';

interface IGlobalState {
  user: userType;
  role: roleType;
}

export class GlobalStore extends MethodStore<IGlobalState> {
  get user() {
    return this.get('user');
  }

  set user(value: userType) {
    this.set('user', value);
  }

  get role() {
    return this.get('role');
  }

  set role(value: roleType) {
    this.set('role', value);
  }
}
