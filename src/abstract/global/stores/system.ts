import { MethodStore } from '../redux-helper';

interface ISystemState {
  loadingHash: string[];
  loadingTitle: string;
  title: string;
}

export class SystemStore extends MethodStore<ISystemState> {
  get loadingHash() {
    return this.get('loadingHash') || [];
  }

  set loadingHash(value: string[]) {
    this.set('loadingHash', value);
  }

  get loadingTitle() {
    return this.get('loadingTitle') || '';
  }

  set loadingTitle(value: string) {
    this.set('loadingTitle', value);
  }

  get title() {
    return this.get('title') || '';
  }

  set title(value: string) {
    this.set('title', value);
  }
}
