import { IStorageAdapter } from '../storage';

const config: IStorageAdapter = {
  async getItem(key: string) {
    return localStorage.getItem(key);
  },
  async setItem(key: string, value: string) {
    return localStorage.setItem(key, value);
  },
  async removeItem(key: string) {
    return localStorage.removeItem(key);
  }
};

export default config;
