import { Storage } from './storage';

export interface IAction {
  type: string;
  [key: string]: any;
}
export type IReducer<State> = (state: State, action: IAction) => State;
export type ISubscriber<State> = (newState: State) => any;

export interface IStoreAdapter<State> {
  getState: () => State;
  dispatch: (action: IAction) => void;
  subscribe: (listener: () => void) => (() => void);
  replaceReducer: (reducer: IReducer<State>) => void;
}

export type StoreAdapterFabric<State> = (
  reducer: IReducer<State>,
  initialState?: any
) => IStoreAdapter<State>;

export class Store<State = any> {
  protected _storeAdapterFabric: StoreAdapterFabric<State> = null;
  protected _storeAdapter: IStoreAdapter<State> = null;
  protected _reducers: IReducer<State>[] = [];
  protected _callbacks: ISubscriber<State>[] = [];
  protected _stateSymbol: symbol = Symbol('state');
  protected _storage: Storage = null;

  protected storeWillMount() {
    // store will mount event
  }

  protected storeDidMount() {
    // store did mount event
  }

  protected storeWillUnmount() {
    // store will unmount event
  }

  constructor(
    storeAdapterFabric: StoreAdapterFabric<State>,
    reducers: IReducer<State>[],
    initialState?: State
  ) {
    this.storeWillMount = this.storeWillMount.bind(this);
    this.storeDidMount = this.storeDidMount.bind(this);
    this.storeWillUnmount = this.storeWillUnmount.bind(this);
    this.dispatch = this.dispatch.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.get = this.get.bind(this);
    this.set = this.set.bind(this);

    this._storeAdapterFabric = storeAdapterFabric;
    this._reducers = reducers || [];
    this.state = Object.assign({}, initialState);
    this._storeAdapter = this._storeAdapterFabric(
      (state: State, action: IAction) => {
        let newState = Object.assign({}, state);
        for (let reducer of this._reducers) {
          newState = reducer(newState, action);
        }
        return newState;
      },
      initialState
    );
    this.storeWillMount();
  }

  setStorage(storage: Storage) {
    this._storage = storage;
    return this;
  }

  get state() {
    return this[this._stateSymbol];
  }

  set state(value: State) {
    this[this._stateSymbol] = value;
  }

  dispatch<T extends IAction>(action: T) {
    return this._storeAdapter.dispatch(action);
  }

  subscribe(config: ISubscriber<State>) {
    const listener = this._storeAdapter.subscribe(() => {
      this.state = this._storeAdapter.getState();
      config(this.state);
      if (this._storage) {
        this._storage.save(this.state);
      }
    });
    this._callbacks.push(config);
    if (this._callbacks.length == 1) this.storeDidMount();
    return () => {
      listener();
      this._callbacks = this._callbacks.filter(m => m !== config);
      if (this._callbacks.length == 0) this.storeWillUnmount();
    };
  }

  async load() {
    if (this._storage) {
      this.state =
        (await this._storage.load()) ||
        this.state ||
        this._storeAdapter.getState();
    }
  }

  async clear() {
    if (this._storage) {
      await this._storage.clear();
    }
  }

  get<Key extends keyof State>(key: Key) {
    return this.state[key];
  }

  set<Key extends keyof State>(key: Key, value: State[Key]) {
    this.state[key] = value;
  }
}
