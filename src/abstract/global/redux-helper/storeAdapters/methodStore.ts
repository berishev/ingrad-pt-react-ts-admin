import guid from 'berish-guid';
import { IAction, Store } from '../store';

export type IMethodReducer<State, TStore extends MethodStore<State>> = (
  store: TStore,
  state: State,
  action: IAction
) => TStore | void;

export interface IMethodAction<State, TStore extends MethodStore<State>>
  extends IAction {
  reducer: IMethodReducer<State, TStore>;
}

export class MethodStore<State = any> extends Store<State> {
  _methodAction = guid.guid();

  storeWillMount() {
    super.storeWillMount();

    this.isMethodAction = this.isMethodAction.bind(this);
    this.methodReduce = this.methodReduce.bind(this);
    this.createMethod = this.createMethod.bind(this);

    this._reducers = [this.methodReduce, ...this._reducers];
  }

  isMethodAction(action: IAction): action is IMethodAction<State, this> {
    if (action.type == this._methodAction) return true;
    return false;
  }

  methodReduce(state: State, action: IAction) {
    if (this.isMethodAction(action)) {
      const self = action.reducer(this, state, action) || this;
      return Object.assign({}, self.state);
    }
    return state;
  }

  createMethod(
    methodReducer: IMethodReducer<State, this>
  ): IMethodAction<State, this> {
    return { type: this._methodAction, reducer: methodReducer };
  }
}
