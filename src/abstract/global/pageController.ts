import { IContentRouteProps } from '../router/contentRoute';
import { NavigatorController } from './navigatorController';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { IModalRouteAdditionalProps } from '../router/modalRoute';

export interface IPageControllerProps<MatchGeneric = {}, ResolveGeneric = {}> {
  controller: PageController<MatchGeneric>;
  modal?: IStaticComponentProps<ResolveGeneric> &
    IModalRouteAdditionalProps<MatchGeneric>;
}

export class PageController<MatchGeneric = {}> {
  static init<MatchGeneric>(
    contentRoute: IContentRouteProps<MatchGeneric>,
    controller?: PageController<MatchGeneric>
  ) {
    if (!controller) controller = new PageController();
    return controller.receive(contentRoute);
  }

  private _navigatorController: NavigatorController<MatchGeneric> = null;

  receive(router: IContentRouteProps<MatchGeneric>) {
    this._navigatorController = NavigatorController.init(
      router,
      this._navigatorController
    );
    return this;
  }

  get navigator() {
    return this._navigatorController;
  }
}
