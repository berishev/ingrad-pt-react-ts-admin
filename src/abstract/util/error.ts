import * as collection from 'berish-collection';
import Enum from 'berish-enum';
import { EnumValueOf } from 'berish-enum/dist/lib';

export const LangCode = Enum.createFromPrimitive('RU', 'EN');

export const ErrorTypeCode = Enum.createFromObject({
  OTHER_CAUSE: -1,
  INTERNAL_SERVER_ERROR: 1,
  CONNECTION_FAILED: 100,
  OBJECT_NOT_FOUND: 101,
  INVALID_QUERY: 102,
  INVALID_CLASS_NAME: 103,
  MISSING_OBJECT_ID: 104,
  INVALID_KEY_NAME: 105,
  INVALID_POINTER: 106,
  INVALID_JSON: 107,
  COMMAND_UNAVAILABLE: 108,
  NOT_INITIALIZED: 9,
  INCORRECT_TYPE: 111,
  INVALID_CHANNEL_NAME: 112,
  PUSH_MISCONFIGURED: 115,
  OBJECT_TOO_LARGE: 116,
  OPERATION_FORBIDDEN: 119,
  CACHE_MISS: 120,
  INVALID_NESTED_KEY: 121,
  INVALID_FILE_NAME: 122,
  INVALID_ACL: 123,
  TIMEOUT: 124,
  INVALID_EMAIL_ADDRESS: 125,
  MISSING_CONTENT_TYPE: 126,
  MISSING_CONTENT_LENGTH: 127,
  INVALID_CONTENT_LENGTH: 128,
  FILE_TOO_LARGE: 129,
  FILE_SAVE_ERROR: 130,
  DUPLICATE_VALUE: 137,
  INVALID_ROLE_NAME: 139,
  EXCEEDED_QUOTA: 140,
  SCRIPT_FAILED: 141,
  VALIDATION_ERROR: 142,
  INVALID_IMAGE_DATA: 150,
  UNSAVED_FILE_ERROR: 151,
  INVALID_PUSH_TIME_ERROR: 152,
  FILE_DELETE_ERROR: 153,
  REQUEST_LIMIT_EXCEEDED: 155,
  INVALID_EVENT_NAME: 160,
  USERNAME_MISSING: 200,
  PASSWORD_MISSING: 201,
  USERNAME_TAKEN: 202,
  EMAIL_TAKEN: 203,
  EMAIL_MISSING: 204,
  EMAIL_NOT_FOUND: 205,
  SESSION_MISSING: 206,
  MUST_CREATE_USER_THROUGH_SIGNUP: 207,
  ACCOUNT_ALREADY_LINKED: 208,
  INVALID_SESSION_TOKEN: 209,
  LINKED_ID_MISSING: 250,
  INVALID_LINKED_SESSION: 251,
  UNSUPPORTED_SERVICE: 252,
  AGGREGATE_ERROR: 600,
  FILE_READ_ERROR: 601,
  X_DOMAIN_REQUEST: 602,
  ACCESS_DISABLED: 1000
});

const ErrorRULabels = new collection.Dictionary<EnumValueOf<typeof ErrorTypeCode>, string>(
  new collection.KeyValuePair(ErrorTypeCode.OTHER_CAUSE, 'Неизвестная ошибка'),
  new collection.KeyValuePair(ErrorTypeCode.INTERNAL_SERVER_ERROR, 'Внутренняя ошибка сервера'),
  new collection.KeyValuePair(ErrorTypeCode.CONNECTION_FAILED, 'Соединение не выполнено'),
  new collection.KeyValuePair(ErrorTypeCode.OBJECT_NOT_FOUND, 'Объект не найден'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_QUERY, 'Неверный запрос'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CLASS_NAME, 'Неверное имя класса'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_OBJECT_ID, 'Отсутствует ID объекта'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_KEY_NAME, 'Неверное имя ключа'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_POINTER, 'Неверный указатель'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_JSON, 'Неверный JSON'),
  new collection.KeyValuePair(ErrorTypeCode.COMMAND_UNAVAILABLE, 'Недоступная команда'),
  new collection.KeyValuePair(ErrorTypeCode.NOT_INITIALIZED, 'Не инициализирован'),
  new collection.KeyValuePair(ErrorTypeCode.INCORRECT_TYPE, 'Некорректный тип'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CHANNEL_NAME, 'Неверное имя канала'),
  new collection.KeyValuePair(ErrorTypeCode.PUSH_MISCONFIGURED, 'Пуш несконфигурирован'),
  new collection.KeyValuePair(ErrorTypeCode.OBJECT_TOO_LARGE, 'Объект слишком большой'),
  new collection.KeyValuePair(ErrorTypeCode.OPERATION_FORBIDDEN, 'Операция запрещена'),
  new collection.KeyValuePair(ErrorTypeCode.CACHE_MISS, 'Ошибка кэширования'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_NESTED_KEY, 'Недопустимый вложенный ключ'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_FILE_NAME, 'Неверное имя файла'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_ACL, 'Неверный ACL'),
  new collection.KeyValuePair(ErrorTypeCode.TIMEOUT, 'Тайм-аут'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_EMAIL_ADDRESS, 'Неверный адрес электронной почты'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_CONTENT_TYPE, 'Отсутствует тип содержимого'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_CONTENT_LENGTH, 'Отсутствует длина содержимого'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CONTENT_LENGTH, 'Недопустимая длина содержимого'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_TOO_LARGE, 'Файл слишком большой'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_SAVE_ERROR, 'Ошибка сохранения файла'),
  new collection.KeyValuePair(ErrorTypeCode.DUPLICATE_VALUE, 'Дублирующее значение'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_ROLE_NAME, 'Недопустимое имя роли'),
  new collection.KeyValuePair(ErrorTypeCode.EXCEEDED_QUOTA, 'Превышенная квота'),
  new collection.KeyValuePair(ErrorTypeCode.SCRIPT_FAILED, 'Ошибка скрипта'),
  new collection.KeyValuePair(ErrorTypeCode.VALIDATION_ERROR, 'Ошибка проверки'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_IMAGE_DATA, 'Недопустимые данные изображения'),
  new collection.KeyValuePair(ErrorTypeCode.UNSAVED_FILE_ERROR, 'Ошибка несохраненного файла'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_PUSH_TIME_ERROR, 'Ошибка неверного времени пуша'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_DELETE_ERROR, 'Ошибка удаления файла'),
  new collection.KeyValuePair(ErrorTypeCode.REQUEST_LIMIT_EXCEEDED, 'Превышен лимит запроса'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_EVENT_NAME, 'Недопустимое имя события'),
  new collection.KeyValuePair(ErrorTypeCode.USERNAME_MISSING, 'Имя пользователя отсутствует'),
  new collection.KeyValuePair(ErrorTypeCode.PASSWORD_MISSING, 'Отсутствует пароль'),
  new collection.KeyValuePair(ErrorTypeCode.USERNAME_TAKEN, 'Такой пользователь уже существует'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_TAKEN, 'Письмо получено'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_MISSING, 'Электронная почта отсутствует'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_NOT_FOUND, 'Электронная почта не подтверждена'),
  new collection.KeyValuePair(ErrorTypeCode.SESSION_MISSING, 'Отсутствует сессия'),
  new collection.KeyValuePair(ErrorTypeCode.MUST_CREATE_USER_THROUGH_SIGNUP, 'Необходимо создать пользователя через регистрацию'),
  new collection.KeyValuePair(ErrorTypeCode.ACCOUNT_ALREADY_LINKED, 'Учетная запись уже связана'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_SESSION_TOKEN, 'Недопустимый токен сеанса'),
  new collection.KeyValuePair(ErrorTypeCode.LINKED_ID_MISSING, 'Связанный идентификатор отсутствует'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_LINKED_SESSION, 'Недействительный сеанс связи'),
  new collection.KeyValuePair(ErrorTypeCode.UNSUPPORTED_SERVICE, 'Неподдерживаемый сервис'),
  new collection.KeyValuePair(ErrorTypeCode.AGGREGATE_ERROR, 'Совокупная ошибка'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_READ_ERROR, 'Ошибка чтения файла'),
  new collection.KeyValuePair(ErrorTypeCode.X_DOMAIN_REQUEST, 'Х-Доменный запрос'),
  new collection.KeyValuePair(ErrorTypeCode.ACCESS_DISABLED, 'Доступ запрещен')
);
const ErrorENLabels = new collection.Dictionary<EnumValueOf<typeof ErrorTypeCode>, string>(
  new collection.KeyValuePair(ErrorTypeCode.OTHER_CAUSE, 'Unknown error'),
  new collection.KeyValuePair(ErrorTypeCode.INTERNAL_SERVER_ERROR, 'Internal server error'),
  new collection.KeyValuePair(ErrorTypeCode.CONNECTION_FAILED, 'Connection failed'),
  new collection.KeyValuePair(ErrorTypeCode.OBJECT_NOT_FOUND, 'Object not found'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_QUERY, 'Invalid query'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CLASS_NAME, 'Invalid class name'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_OBJECT_ID, 'Missing object ID'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_KEY_NAME, 'Invalid key name'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_POINTER, 'Invalid pointer'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_JSON, 'Invalid JSON'),
  new collection.KeyValuePair(ErrorTypeCode.COMMAND_UNAVAILABLE, 'Command unavailable'),
  new collection.KeyValuePair(ErrorTypeCode.NOT_INITIALIZED, 'Not initialized'),
  new collection.KeyValuePair(ErrorTypeCode.INCORRECT_TYPE, 'Incorrect type'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CHANNEL_NAME, 'Invalid channel name'),
  new collection.KeyValuePair(ErrorTypeCode.PUSH_MISCONFIGURED, 'Push misconfigured'),
  new collection.KeyValuePair(ErrorTypeCode.OBJECT_TOO_LARGE, 'Object is too big'),
  new collection.KeyValuePair(ErrorTypeCode.OPERATION_FORBIDDEN, 'Operation not allowed'),
  new collection.KeyValuePair(ErrorTypeCode.CACHE_MISS, 'Cache is missed'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_NESTED_KEY, 'Invalid nested key'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_FILE_NAME, 'Invalid file name'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_ACL, 'Invalid ACL'),
  new collection.KeyValuePair(ErrorTypeCode.TIMEOUT, 'Timeout'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_EMAIL_ADDRESS, 'Incorrect E-Mail Address'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_CONTENT_TYPE, 'Missing content type'),
  new collection.KeyValuePair(ErrorTypeCode.MISSING_CONTENT_LENGTH, 'Missing content length'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_CONTENT_LENGTH, 'Invalid content length'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_TOO_LARGE, 'File too large'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_SAVE_ERROR, 'Error saving file'),
  new collection.KeyValuePair(ErrorTypeCode.DUPLICATE_VALUE, 'Duplicate value'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_ROLE_NAME, 'Invalid role name'),
  new collection.KeyValuePair(ErrorTypeCode.EXCEEDED_QUOTA, 'Exceeded quota'),
  new collection.KeyValuePair(ErrorTypeCode.SCRIPT_FAILED, 'Script Error'),
  new collection.KeyValuePair(ErrorTypeCode.VALIDATION_ERROR, 'Validation error'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_IMAGE_DATA, 'Invalid image data'),
  new collection.KeyValuePair(ErrorTypeCode.UNSAVED_FILE_ERROR, 'Unsaved file error'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_PUSH_TIME_ERROR, 'Incorrect push-up time error'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_DELETE_ERROR, 'Error deleting file'),
  new collection.KeyValuePair(ErrorTypeCode.REQUEST_LIMIT_EXCEEDED, 'Request limit exceeded'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_EVENT_NAME, 'Invalid event name'),
  new collection.KeyValuePair(ErrorTypeCode.USERNAME_MISSING, 'No user name'),
  new collection.KeyValuePair(ErrorTypeCode.PASSWORD_MISSING, 'Missing password'),
  new collection.KeyValuePair(ErrorTypeCode.USERNAME_TAKEN, 'Username taken'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_TAKEN, 'Email received'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_MISSING, 'Email is missing'),
  new collection.KeyValuePair(ErrorTypeCode.EMAIL_NOT_FOUND, 'Email not found'),
  new collection.KeyValuePair(ErrorTypeCode.SESSION_MISSING, 'Missing session'),
  new collection.KeyValuePair(ErrorTypeCode.MUST_CREATE_USER_THROUGH_SIGNUP, 'You must create a user through registration'),
  new collection.KeyValuePair(ErrorTypeCode.ACCOUNT_ALREADY_LINKED, 'Account already linked'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_SESSION_TOKEN, 'Invalid session token'),
  new collection.KeyValuePair(ErrorTypeCode.LINKED_ID_MISSING, 'Linked ID is missing'),
  new collection.KeyValuePair(ErrorTypeCode.INVALID_LINKED_SESSION, 'Invalid lenked session'),
  new collection.KeyValuePair(ErrorTypeCode.UNSUPPORTED_SERVICE, 'Unsupported service'),
  new collection.KeyValuePair(ErrorTypeCode.AGGREGATE_ERROR, 'Agregate error'),
  new collection.KeyValuePair(ErrorTypeCode.FILE_READ_ERROR, 'Error reading file'),
  new collection.KeyValuePair(ErrorTypeCode.X_DOMAIN_REQUEST, 'X-domain request'),
  new collection.KeyValuePair(ErrorTypeCode.ACCESS_DISABLED, 'Access disabled')
);

const ErrorLabels = new collection.Dictionary(new collection.KeyValuePair(LangCode.RU, ErrorRULabels), new collection.KeyValuePair(LangCode.EN, ErrorENLabels));

export class SystemError extends Error {
  public lang: EnumValueOf<typeof LangCode> = null;
  public code: EnumValueOf<typeof ErrorTypeCode> = null;

  constructor(code: EnumValueOf<typeof ErrorTypeCode> | string, lang?: EnumValueOf<typeof LangCode>) {
    super(typeof code == 'string' ? code : getError(code, lang));
    this.name = 'SystemError';
    this.lang = lang || LangCode.EN;
    this.code = typeof code == 'string' ? ErrorTypeCode.OTHER_CAUSE : code;
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, SystemError);
    } else {
      this.stack = new Error().stack;
    }
  }
}

export function getError(code: EnumValueOf<typeof ErrorTypeCode>, lang?: EnumValueOf<typeof LangCode>) {
  const getError = (code: EnumValueOf<typeof ErrorTypeCode>, lang: EnumValueOf<typeof LangCode>): string => {
    let linq = ErrorLabels.toLinq()
      .select(m => {
        let linq = m.value.toLinq();
        let kv = linq.firstOrNull(m => m.key == code);
        return {
          lang: m.key,
          error: kv && kv.value
        };
      })
      .notNull();
    let error = linq.firstOrNull(m => m.lang == lang);
    if (error == null) error = linq.firstOrNull(m => m.lang == LangCode.EN);
    if (error == null) error = linq.firstOrNull();
    return (error && error.error) || getError(ErrorTypeCode.OTHER_CAUSE, lang);
  };
  return getError(code, lang == null ? LangCode.RU : lang);
}
