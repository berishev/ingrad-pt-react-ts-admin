export function getRecursiveEl(element: Element, className: string) {
  function load(element: Element | Node) {
    if (element == null) return null;
    let parent = element.parentNode;
    if (parent == null) return null;
    if (parent.isEqualNode(document.body)) return null;
    if (parent['className'] == className) return parent;
    return load(parent);
  }
  return load(element);
}
