import {
  promiseGet,
  ObjectOrFunctionOrPromiseOrFunctionOfPromise
} from 'berish-promise-getter';

type KeyValuePair<Value = any> = { [key: string]: Value };
interface IRouteQueryProps<T = any> {
  condition: ObjectOrFunctionOrPromiseOrFunctionOfPromise<boolean>;
  meta?: T;
}

export class RouteQuery<T extends KeyValuePair = KeyValuePair> {
  private _meta: T = null;
  private _condition: ObjectOrFunctionOrPromiseOrFunctionOfPromise<
    boolean
  > = null;

  getMeta() {
    return this._meta || ({} as T);
  }

  constructor(params: IRouteQueryProps<T>) {
    this._condition = params.condition;
    this._meta = params.meta || ({} as T);
  }

  static or(...queries: RouteQuery[]) {
    let promise = () =>
      new Promise<boolean>(async (resolve, reject) => {
        for (let query of queries) {
          let response = await query.execute();
          let { result, meta } = response;
          if (result) {
            resolve(true);
          }
        }
        resolve(false);
      });
    let meta: KeyValuePair = {};
    for (let query of queries) {
      meta = Object.assign({}, meta, query.getMeta());
    }
    return new RouteQuery({ condition: promise, meta });
  }

  static and(...queries: RouteQuery[]) {
    let promise = () =>
      new Promise<boolean>(async (resolve, reject) => {
        for (let query of queries) {
          let response = await query.execute();
          let { result, meta } = response;
          if (!result) {
            resolve(false);
          }
        }
        resolve(true);
      });
    let meta: KeyValuePair = {};
    for (let query of queries) {
      meta = Object.assign({}, meta, query.getMeta());
    }
    return new RouteQuery({ condition: promise, meta });
  }

  static condition(
    value: ObjectOrFunctionOrPromiseOrFunctionOfPromise<boolean>
  ) {
    return new RouteQuery({ condition: value });
  }

  meta<K extends KeyValuePair = KeyValuePair>(meta: K) {
    this._meta = Object.assign({}, this._meta, meta);
    return (this as any) as RouteQuery<T & K>;
  }

  async execute() {
    let result = await promiseGet(this._condition);
    let meta = this.getMeta();
    return {
      result,
      meta
    };
  }
}
