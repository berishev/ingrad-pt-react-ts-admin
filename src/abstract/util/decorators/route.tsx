import { roleType, userType, getCurrentUser, getCurrentRole } from '../auth';
import * as Model from '../../../model';
import * as collection from 'berish-collection/dist';
import { LINQ } from 'berish-linq/dist';
import { RouteQuery } from './query';
import { ObjectOrFunctionOrPromiseOrFunctionOfPromise, promiseGet } from 'berish-promise-getter/dist';

const ROUTEPATH_KEY = 'routePath';
const NOTUSEDAREAVIEW_KEY = 'notUsedAreaView';
const ROUTEQUERY_KEY = 'routeQuery';

export function RoutePath(route: string) {
  return function(Component: any) {
    Component[ROUTEPATH_KEY] = route;
    return Component;
  };
}

export function DNotUsedAreaView(Component: any) {
  Component[NOTUSEDAREAVIEW_KEY] = true;
  return Component;
}
export function DRouteQuery(query: RouteQuery) {
  return function(Component) {
    Component[ROUTEQUERY_KEY] = query;
    return Component;
  };
}

export function DUserRoute(cb: (user: userType) => ObjectOrFunctionOrPromiseOrFunctionOfPromise<boolean>, redirect?: string) {
  let promise = () =>
    new Promise<boolean>(async resolve => {
      let user = await getCurrentUser();
      let result = await promiseGet(cb(user));
      resolve(result);
    });
  return new RouteQuery({
    condition: promise,
    meta: { redirect }
  });
}

export function DRoleRoute(cb: (role: roleType) => ObjectOrFunctionOrPromiseOrFunctionOfPromise<boolean>, redirect?: string) {
  let promise = () =>
    new Promise<boolean>(async resolve => {
      let role = await getCurrentRole();
      let result = await promiseGet(cb(role));
      return result;
    });
  return new RouteQuery({
    condition: promise,
    meta: { redirect }
  });
}

export function DPublicRoute() {
  return new RouteQuery({
    condition: true
  });
}

export function DNotAuthorized() {
  return DUserRoute(m => m == null, '/');
}

export function getNotUsedAreaView(Component: any) {
  return ((Component && Component[NOTUSEDAREAVIEW_KEY]) || null) as boolean;
}

export function getRouteQuery(Component) {
  return ((Component && Component[ROUTEQUERY_KEY]) || null) as RouteQuery;
}

export function getRoutePath(Component) {
  return ((Component && Component[ROUTEPATH_KEY]) || null) as string;
}
