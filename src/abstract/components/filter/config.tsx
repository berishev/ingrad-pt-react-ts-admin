import { LINQ } from 'berish-linq';
import Decorators from 'berish-decorate';
import { cloneDeep } from 'lodash';

export type Object_Array_LINQ<T> = LINQ<T> | T[] | T;
export type Object_PromiseOfObject<T> = T | Promise<T>;

export default class AbstractConfig<
  ConfigObject,
  FilterRequest = ConfigObject,
  FilterResponse = ConfigObject
> {
  public collapsed = false;

  protected symbolCacheResponse = Symbol('cacheResponse');
  protected symbolAttributes = Symbol('attributes');

  private requestCall: () => ConfigObject = null;

  constructor(requestCall: () => ConfigObject) {
    this.requestCall = requestCall;
    setImmediate(() => this.apply());
  }

  // Его слушает контроллер, когда рендерим
  get response() {
    return (
      (this[this.symbolCacheResponse] as ConfigObject) || this.requestCall()
    );
  }

  apply() {
    this.cleanCache();
    let response = this.applyMethod();
    this[this.symbolCacheResponse] = response;
    return this;
  }

  cleanCache() {
    this[this.symbolCacheResponse] = undefined;
    this.symbolCacheResponse = Symbol('response');
    return this;
  }

  cleanAttributes() {
    this[this.symbolAttributes] = undefined;
    this.symbolAttributes = Symbol('attributes');
    return this;
  }

  // Зависит от реализации в детях
  protected applyMethod() {
    return this.clone(this.requestCall());
  }

  get filters(): ((object: FilterRequest) => FilterResponse)[] {
    return [];
  }

  get attributes() {
    if (!this[this.symbolAttributes]) this[this.symbolAttributes] = {};
    return this[this.symbolAttributes];
  }

  protected get(key: string) {
    return this.attributes[key];
  }

  protected set(key: string, value: any) {
    this.attributes[key] = value;
  }

  // Helpers

  protected clone<T>(data: T) {
    return AbstractConfig.clone(data);
  }

  static clone<T>(data: T) {
    return cloneDeep(data);
  }

  protected getLinq<T>(obj: Object_Array_LINQ<T>): LINQ<T> {
    if (obj instanceof LINQ) return obj;
    if (Array.isArray(obj)) return LINQ.fromArray(obj);
    return this.getLinq([obj]);
  }

  static filterProperty(defaultValue?: any) {
    return function(target: any, key: string) {
      let descriptor = Object.getOwnPropertyDescriptor(target, key) || {};
      delete target[key];
      descriptor.set = function(val) {
        let self = (this as any) as AbstractConfig<any>;
        self.set(key, val);
      };
      descriptor.get = function() {
        let self = (this as any) as AbstractConfig<any>;
        let value = self.get(key);
        return typeof value == 'undefined' ? defaultValue : value;
      };
      Object.defineProperty(target, key, descriptor);
    };
  }
}

Decorators.propertyDescriptor(AbstractConfig, 'collapsed', [
  AbstractConfig.filterProperty(false)
]);
