import * as Parse from 'parse';
import { LINQ } from 'berish-linq';
import AbstractConfig, { Object_Array_LINQ } from '../config';

type ConfigObject<ParseObject extends Parse.Object> = Parse.Query<ParseObject>;
type FilterRequest<ParseObject extends Parse.Object> = Parse.Query<ParseObject>;
type FilterResponse<ParseObject extends Parse.Object> = Parse.Query<
  ParseObject
>;

export default class ParseAbstractConfig<
  ParseObject extends Parse.Object
> extends AbstractConfig<
  ConfigObject<ParseObject>,
  FilterRequest<ParseObject>,
  FilterResponse<ParseObject>
> {
  protected applyMethod() {
    let result = super.applyMethod();
    let queries: FilterResponse<ParseObject>[] = [];
    if (this.filters != null && this.filters.length > 0) {
      let filters = this.filters;
      queries = filters.map(filter => filter(this.clone(result)));
    }
    return queries.length <= 1 ? queries[0] : this.parseAnd(...queries);
  }

  protected parseOr<T extends Parse.Object>(...data: Parse.Query<T>[]) {
    return ParseAbstractConfig.parseOr(...data);
  }

  protected parseAnd<T extends Parse.Object>(...data: Parse.Query<T>[]) {
    return ParseAbstractConfig.parseAnd(...data);
  }

  protected parseGetIncludes(...data: Parse.Query[]) {
    return ParseAbstractConfig.parseGetIncludes(...data);
  }

  static parseGetIncludes(...data: Parse.Query[]) {
    return LINQ.fromArray(data || [])
      .selectMany(m => (m['_include'] || []) as string[])
      .distinct()
      .toArray();
  }

  static parseOr<T extends Parse.Object>(...data: Parse.Query<T>[]) {
    let includes = this.parseGetIncludes(...data);
    let query = Parse.Query.or(...data);
    if (includes.length > 0) query = query.include(includes);
    return query;
  }

  static parseAnd<T extends Parse.Object>(...data: Parse.Query<T>[]) {
    let includes = this.parseGetIncludes(...data);
    let query = Parse.Query['and'](...data) as Parse.Query<T>;
    if (includes.length > 0) query = query.include(includes);
    return query;
  }
}
