import * as React from 'react';
import { AbstractComponent, FormItem } from './abstract';
import TextField, { ITextFieldProps } from './textfield';

export interface IPhoneFieldProps {}

const regExp = /^\+?[8,7]\d{0,10}$/;

class PhoneField extends AbstractComponent<
  IPhoneFieldProps & ITextFieldProps,
  {}
> {
  constructor(props) {
    super(props);
  }

  render() {
    return <TextField {...this.props} regExp={regExp} fiDisable={true} />;
  }
}

export default FormItem(PhoneField, { fiTitleType: 'InputLabel' });
