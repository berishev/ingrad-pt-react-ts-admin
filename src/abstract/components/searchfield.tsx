import * as React from 'react';
import { AbstractComponent, FormItem } from './abstract';
import TextField, { ITextFieldProps } from './textfield';

export interface ISeachfieldProps extends ITextFieldProps {}

class Seachfield extends AbstractComponent<ISeachfieldProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    return <TextField {...this.props} fiDisable={true} />;
  }
}

export default FormItem(Seachfield, { fiTitleType: 'InputLabel' });
