import * as React from 'react';
import { Modal, WithStyles, withStyles } from '@material-ui/core';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { storageController } from '../../global';
import styles from './styles';
import Spin from '../spin';

export interface IModalFormProps extends IStaticComponentProps<null> {
  width?: string | number;
}

interface IModalFormState {
  loading: boolean;
}

class ModalForm extends React.Component<
  IModalFormProps & WithStyles<typeof styles>,
  IModalFormState
> {
  unlistener = storageController.systemStore.subscribe(m => {
    let loading = !!(m.loadingHash && m.loadingHash.length > 0);
    if (loading != this.state.loading) this.setState({ loading });
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  render() {
    const { classes } = this.props;
    return (
      <Modal
        open={true}
        title={undefined}
        container={
          this.props.getContainer ? this.props.getContainer(null) : null
        }
        onClose={e => this.props.reject && this.props.reject()}
      >
        <div
          style={{
            maxWidth: '90vw',
            maxHeight: '80vh',
            overflowY: 'auto',
            width: this.props.width,
            top: `50%`,
            left: `50%`,
            transform: `translate(-50%, -50%)`
          }}
          className={classes.paper}
        >
          <Spin loading={this.state.loading}>{this.props.children}</Spin>
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(ModalForm) as React.ComponentClass<
  IModalFormProps
>;
