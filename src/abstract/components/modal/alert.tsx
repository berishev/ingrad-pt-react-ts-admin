import * as React from 'react';
import ModalForm from './form';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { ContentForm, ButtonBar, Button, Row, Col } from '../';

export interface IAlertModalProps extends IStaticComponentProps<boolean> {
  text: string;
  title: string;
  type: 'warningIcon';
  okTitle?: string;
  cancelTitle?: string;
}

export default class AlertModal extends React.Component<IAlertModalProps> {
  renderContent = () => {
    return <span>{this.props.text}</span>;
  };

  resolve = () => {
    this.props.resolve(true);
  };

  reject = () => {
    this.props.resolve(false);
  };

  renderFooter = () => {
    let left = [<Button key={0} fiTitle={this.props.okTitle || 'Да'} onClick={() => this.resolve()} />, <Button key={1} fiTitle={this.props.cancelTitle || 'Нет'} onClick={() => this.reject()} />];
    return <ButtonBar left={left} />;
  };

  render() {
    return (
      <ModalForm resolve={this.resolve} reject={this.reject} width="40vw">
        <ContentForm title={this.props.title} footerBar={this.renderFooter()}>
          <Row wrap="wrap" style={{ justifyContent: 'center', alignItems: 'center', textAlign: 'center', margin: 10 }}>
            <Col style={{ margin: 5 }}>
              <img src={`/images/${this.props.type}.png`} />
            </Col>
            <Col style={{ margin: 5 }}>{this.props.text}</Col>
          </Row>
        </ContentForm>
      </ModalForm>
    );
  }
}
