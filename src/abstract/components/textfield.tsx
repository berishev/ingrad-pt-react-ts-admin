import * as React from 'react';
import { Input } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import {
  AbstractComponent,
  Tooltip,
  IFormItemProps,
  FormItem
} from './abstract';
import { InputProps } from '@material-ui/core/Input';

export interface ITextFieldProps extends IFormItemProps {
  placeholder?: string;
  regExp?: RegExp;
  onFocus?: () => void;
  onBlur?: () => void;
  type?: 'text' | 'area' | 'password';
  usePasswordHint?: boolean;
  onPressEnter?: () => void;
  disabled?: boolean;
}

interface ITextfieldState {
  value: string;
}

class Textfield extends AbstractComponent<ITextFieldProps, ITextfieldState> {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  componentDidMount() {
    let value = this.getValue() || '';
    if (value != this.state.value) this.setState({ value });
  }

  componentWillReceiveProps(nextProps: ITextFieldProps) {
    let value = this.getValue(null, nextProps) || '';
    if (value != this.state.value) this.setState({ value });
  }

  clearValue = () => {
    this.onChange('');
  };

  onChange = (value: string) => {
    let regExp = this.props.regExp;
    if (regExp) {
      if (!(regExp.test(value) || value === '')) {
        return;
      }
    }
    this.setValue((value || '').replace(/^\s+/, ''));
  };

  renderSuffix = (value: any) => {
    return value ? <Close onClick={this.clearValue} /> : null;
  };

  render() {
    const { value } = this.state;
    let placeholder = this.props.placeholder;
    let type = this.props.type || 'text';
    let props: InputProps = {
      placeholder: this.props.usePasswordHint
        ? '********'
        : placeholder || null,
      value,
      onChange: e => this.onChange(e.target.value),
      onFocus: this.props.onFocus,
      onBlur: this.props.onBlur,
      onKeyPress: e =>
        e.charCode == 13
          ? this.props.onPressEnter && this.props.onPressEnter()
          : true,
      disabled: !!this.props.disabled,
      type: type == 'password' ? 'password' : undefined,
      multiline: type == 'area' ? true : undefined,
      endAdornment: !!this.props.disabled ? undefined : this.renderSuffix(value)
    };
    let input = <Input {...props} fullWidth={true} />;
    return Tooltip(input, {
      placeholder,
      trigger: 'hover'
    });
  }
}

export default FormItem(Textfield, { fiTitleType: 'InputLabel' });
