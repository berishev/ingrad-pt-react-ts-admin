import * as React from 'react';

interface IDividerProps {
  style?: React.CSSProperties;
  isHrTag?: boolean;
}

export default class Divider extends React.Component<IDividerProps, null> {
  render() {
    let { style, isHrTag } = this.props;
    if (isHrTag) {
      return (
        <hr
          style={{
            margin: '-1px 0px 0px',
            height: '1px',
            border: 'none',
            backgroundColor: 'rgb(224,224,224)',
            width: '100%',
            ...(style || {})
          }}
        />
      );
    }
    return (
      <div style={style} className="divider">
        {this.props.children ? (
          <span className="divider__label">{this.props.children}</span>
        ) : (
          undefined
        )}
      </div>
    );
  }
}
