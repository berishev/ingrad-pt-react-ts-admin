import guid from 'berish-guid';
import AbstractTable, {
  IAbstractTableProps,
  IAbstractTableState,
  ON_PAGE_DEFAULT
} from './abstract';
import { IColumn } from './view';
import parseQueryClone from '../../util/parseQueryClone';
import executeController from '../../global/executeController';
interface IQueryTableProps<T extends Parse.Object>
  extends IAbstractTableProps<Parse.Query<T>> {}
interface IQueryTableState<T> extends IAbstractTableState<T> {}

export default class QueryTable<T extends Parse.Object> extends AbstractTable<
  IQueryTableProps<T>,
  IQueryTableState<T>
> {
  tableHash = guid.guid();
  constructor(props: IQueryTableProps<T>) {
    super(props);
  }

  load = async (nextProps?: IQueryTableProps<T>) => {
    let { data } = nextProps || this.props;
    if (data == null) return;
    let { showData, total, currentPage } = this.state;
    currentPage = 1;
    const executionFunction = async () => {
      let total = await data.count();
      let showData = await this.loadPage(currentPage, nextProps);
      return { total, showData };
    };
    const result = await executeController.start({
      func: executionFunction,
      tag: this.tableHash,
      stopByTag: true
    });
    if (result) {
      total = result.total;
      showData = result.showData;
      this.setState({
        showData,
        total,
        currentPage
      });
    }
  };

  loadPage = async (
    page: number,
    nextProps?: IQueryTableProps<T>,
    pageSize?: number
  ) => {
    let { onPage, data, onChangePage } = nextProps || this.props;
    let { showData } = this.state;
    let limit = pageSize || onPage || ON_PAGE_DEFAULT;
    let query = parseQueryClone(data)
      .skip(limit * (page - 1))
      .limit(limit);
    if (onChangePage) {
      let changedQuery = await onChangePage(query);
      query = parseQueryClone(changedQuery);
    }
    showData = await query.find();
    return showData;
  };

  sortPage = (column: IColumn<any>, type: 'down' | 'up') => {
    const _sortPage = async () => {
      let { data, ...props } = this.props;
      let { showData, currentPage } = this.state;
      let sorterKey = column.sorter();
      data = parseQueryClone(data);
      if (type == 'down') data = data.ascending(sorterKey);
      else if (type == 'up') data = data.descending(sorterKey);
      showData = await this.loadPage(
        currentPage,
        { ...(props as IQueryTableProps<T>), data },
        null
      );
      this.setState({ showData });
    };
    return this.loadData(_sortPage);
  };
}
