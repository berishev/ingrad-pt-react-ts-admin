import * as React from 'react';
import * as _ from 'lodash';
import { Checkbox } from '../';
import TableView, { IColumn } from './view';
import { messagesController } from '../../global';
import { IRawSelection, CellStyleType } from './raw';

export const ON_PAGE_DEFAULT = 1000;

export interface IAbstractTableProps<T> {
  data: T;
  columns: IColumn<any>[];
  onPage?: number;
  onChangePage?: (query: T) => Promise<T>;
  onDoubleClick?: (data: any) => void;
  disabledPagination?: boolean;
  selection?: IRawSelection<any>;
  containerMinusHeight?: number;
  rowStyle?: CellStyleType<T>;
  columnStyle?: CellStyleType<IColumn<T>>;
}

export interface IAbstractTableState<T> {
  loading: boolean;
  showData: T[];
  total: number;
  currentPage: number;
  showAll: boolean;
  onPage: number;
}

export default class AbstractTable<Props extends IAbstractTableProps<any> = IAbstractTableProps<any>, State extends IAbstractTableState<any> = IAbstractTableState<any>> extends React.Component<
  Props,
  State
> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      showData: [],
      total: 0,
      currentPage: 1,
      showAll: false,
      onPage: this.props.onPage || ON_PAGE_DEFAULT
    } as any;
  }

  componentDidMount() {
    this.loadData(this.load);
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.data !== this.props.data) {
      this.loadData(() => this.load(nextProps));
    }
    // if (!_.isEqual(nextProps.data, this.props.data)) this.loadData(() => this.load(nextProps));
  }

  load?: (nextProps?: Props) => any = null;
  loadPage?: (page: number, nextProps?: Props, pageSize?: number, state?: State) => Promise<State['showData']>;
  sortPage?: (column: IColumn<any>, type: 'down' | 'up') => any;

  async loadData<I>(promise: () => I | Promise<I>) {
    const loading = (loading: boolean) => new Promise<void>(resolve => this.setState({ loading }, resolve));
    try {
      await loading(true);
      let result: I = null;
      result = await promise();
      return result;
    } catch (err) {
      messagesController.error(err);
      return null;
    } finally {
      await loading(false);
    }
  }

  // VIEW

  showAll = async () => {
    this.onChange(
      {
        pageSize: this.state.showAll ? this.state.total : this.props.onPage || ON_PAGE_DEFAULT,
        total: this.state.total,
        current: this.state.showAll ? 1 : this.state.currentPage
      },
      [],
      null
    );
  };

  onChange = async (pagination: { total: number; current: number; pageSize: number }, filters: string[], sorter: Object) => {
    let currentPage = pagination.current;
    let pageSize = pagination.pageSize;
    let showData = await this.loadData(() => this.loadPage(currentPage, null, pageSize, this.state));
    this.setState({
      showData,
      currentPage
    });
  };

  rowSelect = (data: any[]) => {
    let onSelect = this.props.selection && this.props.selection.onSelect;
    if (onSelect) this.props.selection.on(data);
  };

  renderAll = (total: number, range: [number, number]) => {
    return (
      <div style={{ margin: '0 5px' }}>
        <Checkbox
          fiTitle="Показать все"
          placeholder="Показать все элементы"
          config={{
            object: this.state,
            path: 'showAll',
            afterSet: state => this.setState(state, this.showAll)
          }}
        />
      </div>
    );
  };

  render() {
    const { showData, total, loading } = this.state;
    return (
      <TableView
        {...this.props}
        loading={loading}
        dataSource={showData || []}
        columns={this.props.columns || []}
        selection={{
          disabled: this.props.selection && this.props.selection.disabled,
          on: this.props.selection && this.props.selection.on,
          items: this.props.selection && this.props.selection.items
        }}
        containerMinusHeight={this.props.containerMinusHeight}
        onSort={this.sortPage}
      />
    );
  }
}
