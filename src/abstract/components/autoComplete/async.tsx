import * as React from 'react';
import View, { IAutoCompleteProps } from './view';
import { AbstractComponent, IFormItemProps } from '../abstract';
import { LINQ } from 'berish-linq';

export interface IAutoCompleteAsyncProps
  extends IFormItemProps,
    IAutoCompleteProps {}

interface IAutoCompleteAsyncPrivateProps extends IAutoCompleteAsyncProps {
  onFetch: (text: string) => Promise<{ value: any; text: string }[]>;
  onSelect: (text: string, value: any) => any;
}

interface IAutoCompleteAsyncState {
  data: { value: any; text: string }[];
  loading: boolean;
}

export default class AutoCompleteAsync extends AbstractComponent<
  IAutoCompleteAsyncPrivateProps,
  IAutoCompleteAsyncState
> {
  constructor(props: IAutoCompleteAsyncPrivateProps) {
    super(props);
    this.state = {
      data: [],
      loading: false
    };
  }

  onFetch = async (text: string) => {
    let data = await this.props.onFetch(text);
    this.setState({ data });
  };

  onChange = (text: string) => {
    if (this.props.onChange) this.props.onChange(text);
    if (this.props.onSelect) {
      let item = LINQ.fromArray(this.state.data).firstOrNull(
        m => m.text == text
      );
      if (item) this.props.onSelect(text, item.value);
    }
  };

  render() {
    return (
      <View
        {...this.props}
        data={this.state.data.map(m => m.text)}
        onFetch={this.onFetch}
        onChange={this.onChange}
      />
    );
  }
}
