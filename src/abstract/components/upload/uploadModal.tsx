import * as React from 'react';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { Modal } from '..';

export interface IUploadModalProps extends IStaticComponentProps {
  url: string;
}

export default class UploadModal extends React.Component<IUploadModalProps> {
  constructor(props) {
    super(props);
  }

  resolve = () => {
    this.props.resolve();
  };

  reject = () => {
    this.resolve();
  };

  render() {
    return (
      <Modal.Form resolve={this.resolve} reject={this.reject} width="40vw">
        <img style={{ width: '100%' }} src={this.props.url} />
      </Modal.Form>
    );
  }
}
