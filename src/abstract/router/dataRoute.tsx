import * as React from 'react';
import { Route } from 'react-router-dom';
import { RouteProps } from 'react-router';
import PermissionRoute from './permissionRoute';
import { parseQueryString } from '../util/parsePath';

export interface IDataRouteProps extends RouteProps {
  component: React.ComponentClass<any>;
  data?: Object;
  convertParams?: (match: any) => any;
  params?: { [key: string]: string | string[] };
}

interface IDataRouteState {}

export default class DataRoute extends React.Component<
  IDataRouteProps,
  IDataRouteState
> {
  constructor(props) {
    super(props);
  }

  render() {
    let { component, data, convertParams } = this.props;
    let path = ((component && component['routePath']) ||
      this.props.path) as string;
    return (
      <Route
        {...this.props}
        path={path}
        component={undefined}
        render={props => {
          let search = props.location.search;
          let paramsString = search && search.substring(1);
          let params = parseQueryString(paramsString);
          return (
            <PermissionRoute
              {...this.props}
              {...props}
              path={path}
              params={params}
            />
          );
        }}
      />
    );
  }
}
