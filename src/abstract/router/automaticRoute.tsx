import * as React from 'react';
import { Switch } from 'react-router';
import { LINQ } from 'berish-linq/dist';
import DataRoute from './dataRoute';
import errorController from '../global/errorController';

export interface IAutomaticRouteProps {
  controllers: { [key: string]: any };
}

export default class AutomaticRoute extends React.Component<
  IAutomaticRouteProps
> {
  constructor(props) {
    super(props);
  }

  generateRoute(
    controllerPath: string,
    methodPath: string,
    controller: React.ComponentClass
  ) {
    controllerPath = controllerPath.toLowerCase();
    methodPath = methodPath.toLocaleLowerCase();
    let fullPath = `${controllerPath}/${methodPath}`;
    if (controller['routePath'] != null) {
      controllerPath = '';
      methodPath = fullPath = controller['routePath'];
    }
    if (controllerPath.indexOf('home') != -1) {
      return (
        <DataRoute
          path={`/${methodPath}`}
          component={controller}
          key={`/${methodPath}`}
        />
      );
    }
    if (methodPath.indexOf('home') != -1) {
      return <DataRoute exact path="/" component={controller} key="/" />;
    }
    return (
      <DataRoute exact path={fullPath} component={controller} key={fullPath} />
    );
  }

  renderRouters() {
    const _render = (
      object: { [key: string]: any },
      path?: string
    ): JSX.Element[] => {
      let keys = Object.keys(object);
      return LINQ.fromArray(keys)
        .selectMany(m => {
          let value = object[m];
          if (typeof value == 'function') {
            return [this.generateRoute(path || '', m, value)];
          }
          return _render(value, `${path || ''}/${m}`);
        })
        .toArray();
    };
    return _render(this.props.controllers);
  }

  render() {
    let ComponentClass = errorController.getErrorPage(404);
    let component = ComponentClass && <DataRoute component={ComponentClass} />;
    return (
      <Switch>
        {this.renderRouters()}
        {component}
      </Switch>
    );
  }
}
