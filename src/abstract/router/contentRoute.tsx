import * as React from 'react';
import AreaView from '../content/areaView';
import { History } from 'history';
import { IPageControllerProps, PageController } from '../global/pageController';
import { getNotUsedAreaView } from '../util/decorators/route';
import { storageController } from '../global';

export interface IContentRouteProps<MatchGeneric> {
  component: React.ComponentClass<IPageControllerProps<MatchGeneric>>;
  params: MatchGeneric;
  history: History;
}

interface IContentRouteState<MatchGeneric> {
  controller: PageController<MatchGeneric>;
}

export default class ContentRoute<
  MatchGeneric = {}
> extends React.PureComponent<
  IContentRouteProps<MatchGeneric>,
  IContentRouteState<MatchGeneric>
> {
  systemUnlistener = storageController.systemStore.subscribe(m =>
    this.forceUpdate()
  );
  localStoreUnlistener = storageController.localStore.subscribe(m =>
    this.forceUpdate()
  );
  globalStoreUnlistener = storageController.globalStore.subscribe(m =>
    this.forceUpdate()
  );

  constructor(props) {
    super(props);
    this.state = {
      controller: PageController.init(this.props)
    };
  }

  componentWillReceiveProps(nextProps: IContentRouteProps<MatchGeneric>) {
    this.setState({
      controller: PageController.init(nextProps, this.state.controller)
    });
  }

  componentWillUnmount() {
    if (this.systemUnlistener) this.systemUnlistener();
    if (this.localStoreUnlistener) this.localStoreUnlistener();
    if (this.globalStoreUnlistener) this.globalStoreUnlistener();
  }

  render() {
    const controller = this.state.controller;
    const ComponentClass = this.props.component;
    const component = <ComponentClass controller={controller} />;
    const notUsedAreaView = getNotUsedAreaView(ComponentClass);
    return notUsedAreaView ? (
      component
    ) : (
      <AreaView controller={controller}>{component}</AreaView>
    );
  }
}
