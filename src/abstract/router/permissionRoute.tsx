import * as React from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import ReactCondition from 'berish-react-condition/dist';
import WithRouter from './withRouter';
import { IDataRouteProps } from './dataRoute';
import * as AuthUtil from '../util/auth';
import messagesController from '../global/messagesController';
import errorController from '../global/errorController';
import {
  DNotUsedAreaView,
  getNotUsedAreaView,
  getRouteQuery
} from '../util/decorators/route';

interface IPermissionRouteState {}

export default class PermissionRoute extends React.Component<
  RouteComponentProps<any> & IDataRouteProps,
  IPermissionRouteState
> {
  private logged = false;
  private decoratorAccess = false;
  private redirect: string = null;
  constructor(props) {
    super(props);
  }

  permissionCheck = async () => {
    let { component } = this.props;
    let routeQuery = getRouteQuery(component);
    if (routeQuery) {
      let response = await routeQuery.execute();
      this.decoratorAccess = response.result;
      this.redirect = response.meta.redirect;
      if (this.decoratorAccess != null) return this.decoratorAccess;
    }
    let logged = await AuthUtil.isLogged();
    this.logged = logged;
    // return this.logged;
    return true;
  };

  render() {
    let ComponentClass403 = errorController.getErrorPage(403);
    let ComponentClassIsNotAuth = errorController.getErrorPage('isNotAuth');
    let ComponentClass = !this.logged
      ? ComponentClassIsNotAuth
      : ComponentClass403;
    if (getNotUsedAreaView(this.props.component)) {
      ComponentClass = DNotUsedAreaView(ComponentClass);
    }
    let component = this.redirect ? (
      <Redirect to={this.redirect} />
    ) : (
      ComponentClass && (
        <WithRouter {...this.props} component={ComponentClass} />
      )
    );
    return (
      <ReactCondition
        condition={this.permissionCheck}
        doNode={<WithRouter {...this.props} />}
        doElseNode={component || <h1>Вам запрещено это делать!</h1>}
        loadingNode={messagesController.loadingGlobal(true)}
      />
    );
  }
}
