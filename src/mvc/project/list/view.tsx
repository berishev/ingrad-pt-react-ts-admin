import * as React from 'react';
import * as Model from '../../../model';
import * as Components from '../../../abstract/components';
import Controller from './controller';
import { IColumn } from '../../../abstract/components/table/view';
import storageController from '../../../abstract/global/storageController';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  constructor(props: IViewProps) {
    super(props);
  }

  renderColumns = () => {
    let columns: IColumn<Model.Project>[] = [
      {
        title: 'Код',
        render: item => item.code
      },
      {
        title: 'Наименование',
        render: item => item.name
      },
      {
        title: 'Коммерческое наименование',
        render: item => item.commercialName
      },
      {
        title: 'Иконка',
        render: item =>
          (item.iconUrl && (
            <Components.Image
              style={{ width: 30, height: 30 }}
              placeholder={item.name || item.code}
              value={item.iconUrl}
            />
          )) ||
          item.code
      },
      {
        title: 'Город',
        render: item =>
          item.relType != null
            ? Model.ProjectRelTypeLabels.get(item.relType)
            : null
      }
    ];
    return columns;
  };

  renderHeader = () => {
    let { state, onAdd, onEdit, onRemove } = this.props.controller;
    let { selected } = state;
    let left = [
      <Components.Button key="1" buttonTitle="Добавить" onClick={onAdd} />,
      <Components.Button
        key="2"
        buttonTitle="Редактировать"
        disabled={selected.length != 1}
        onClick={onEdit}
      />,
      <Components.Button
        key="3"
        buttonTitle="Удалить"
        type="secondary"
        disabled={selected.length < 1}
        onClick={onRemove}
      />
    ];
    return <Components.ButtonBar left={left} />;
  };
  render() {
    let { state, onSelect } = this.props.controller;
    let { query, selected } = state;
    return (
      <Components.ContentForm headerBar={this.renderHeader()}>
        <Components.Table.QueryTable
          data={query}
          columns={this.renderColumns()}
          selection={{ items: selected, on: onSelect }}
          columnStyle={{ fontSize: 19 }}
          rowStyle={{ fontSize: 17 }}
        />
      </Components.ContentForm>
    );
  }
}
