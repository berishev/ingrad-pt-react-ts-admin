import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Root } from 'berish-react-portals/dist';
import AutomaticRoute from '../abstract/router/automaticRoute';
import {
  apiController,
  messagesController,
  storageController
} from '../abstract/global';
import controllers from './modules';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

export default class App extends React.Component<
  any,
  { loaded: boolean; loading: boolean }
> {
  unlistener = storageController.systemStore.subscribe(m => {
    let loading = !!(m.loadingHash && m.loadingHash.length > 0);
    if (loading != this.state.loading) this.setState({ loading });
  });
  constructor(props) {
    super(props);
    this.state = { loaded: false, loading: false };
  }
  async componentWillMount() {
    await apiController.init();
    await storageController.localStore.load();
    this.setState({ loaded: true });
  }

  componentWillUnmount() {
    if (this.unlistener) {
      this.unlistener();
    }
  }

  renderRoutes = () => {
    return messagesController.loadingJSXElement(
      <AutomaticRoute controllers={controllers} />,
      this.state.loading
    );
  };

  render() {
    if (!this.state.loaded) return messagesController.loadingGlobal(true);
    return (
      <BrowserRouter>
        <>
          {this.renderRoutes()}
          <Root />
          <ToastContainer
            position={toast.POSITION.BOTTOM_RIGHT}
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick={true}
            rtl={false}
            draggable={true}
            pauseOnHover={true}
          />
        </>
      </BrowserRouter>
    );
  }
}
