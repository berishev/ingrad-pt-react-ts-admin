import * as React from 'react';
import { IPageControllerProps } from '../../abstract/global/pageController';
import { messagesController } from '../../abstract/global';
import executeController from '../../abstract/global/executeController';

interface IControllerProps extends IPageControllerProps {}

interface IControllerState {}

export default class MainViewController extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    executeController.tryLoad(this.load);
  }

  load = async () => {
    this.pushList();
  };

  pushList = () => {
    this.props.controller.navigator.push('/teamuser/list');
  };

  render() {
    return messagesController.loadingGlobal(true);
  }
}
