import * as React from 'react';
import * as Model from '../../../model';
import * as Components from '../../../abstract/components';
import * as Special from '../../../components';
import Controller from './controller';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  renderPosition = (i: number) => {
    let { state, onChange } = this.props.controller;
    let { item } = state;
    return (
      <Components.Textfield
        fiTitle="Название проектной роли"
        config={{
          object: item,
          path: `positions.${i}`,
          afterSet: onChange
        }}
      />
    );
  };

  renderContent = () => {
    let { state, onChange } = this.props.controller;
    let { item } = state;
    return (
      <>
        <Components.Textfield
          fiTitle="Наименование отдела"
          config={{
            object: item,
            path: 'name',
            afterSet: onChange
          }}
        />
        <Components.Textfield
          fiTitle="Полное наименование отдела"
          config={{
            object: item,
            path: 'fullname',
            afterSet: onChange
          }}
        />
        <Components.Divider>Маппинг</Components.Divider>
        <Special.Selectfield.SPOrgStructureType
          fiTitle="SP-Оргструтура"
          config={{
            object: item,
            path: 'spOrg',
            afterSet: onChange
          }}
        />
        <Components.List
          template={this.renderPosition}
          title="Проектные роли"
          config={{
            object: item,
            path: 'positions',
            afterSet: onChange
          }}
          onAdd={() => {
            item.positions = item.positions.concat('');
            onChange(item);
          }}
          onDelete={(i, m) => {
            item.positions.splice(i, 1);
            onChange(item);
          }}
        />
      </>
    );
  };

  renderHeader = () => {
    let { state, onCancel, onSave } = this.props.controller;
    let left = [
      <Components.Button
        key="1"
        buttonTitle="Сохранить"
        onClick={onSave}
        submit={{ validationObject: state.item, controller: this }}
      />,
      <Components.Button
        key="2"
        buttonTitle="Отменить"
        onClick={onCancel}
        type="secondary"
      />
    ];
    return <Components.ButtonBar left={left} />;
  };

  render() {
    return (
      <Components.ContentForm headerBar={this.renderHeader()}>
        {this.renderContent()}
      </Components.ContentForm>
    );
  }
}
