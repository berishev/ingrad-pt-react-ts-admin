import form from './form/controller';
import list from './list/controller';

export default {
    form, list
}