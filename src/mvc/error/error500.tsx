import * as React from 'react';
import Decorators from 'berish-decorate';
import Exception from './exception';
import { errorController } from '../../abstract/global';
import { IPageControllerProps } from '../../abstract/global/pageController';
import {
  DRouteQuery,
  DPublicRoute,
  DNotUsedAreaView
} from '../../abstract/util/decorators/route';
import * as Components from '../../abstract/components';

class Error500 extends React.Component<IPageControllerProps> {
  constructor(props) {
    super(props);
  }

  onClick = () => {
    this.props.controller.navigator.push('/');
  };

  render() {
    const params = {
      title: '500',
      text: 'Проблема на сервере',
      imgSrc: '/images/x500.svg',
      reactNode: (
        <Components.Button
          type="primary"
          onClick={this.onClick}
          fiTitle="Перейти на главную"
        />
      )
    };
    return <Exception {...params} />;
  }
}
export default Decorators.classDecorate(Error500, [
  DNotUsedAreaView,
  errorController.DRegisterError(500),
  DRouteQuery(DPublicRoute())
]);
