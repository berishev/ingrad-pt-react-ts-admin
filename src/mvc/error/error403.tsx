import * as React from 'react';
import Decorators from 'berish-decorate';
import Exception from './exception';
import { errorController } from '../../abstract/global';
import { IPageControllerProps } from '../../abstract/global/pageController';
import {
  DRouteQuery,
  DPublicRoute,
  DNotUsedAreaView
} from '../../abstract/util/decorators/route';
import * as Components from '../../abstract/components';
import { isLogged } from '../../abstract/util/auth';

class Error403 extends React.Component<
  IPageControllerProps,
  { logged: boolean }
> {
  constructor(props) {
    super(props);
    this.state = {
      logged: null
    };
  }

  async componentDidMount() {
    let logged = await isLogged();
    this.setState({ logged });
  }

  onClick = () => {
    this.props.controller.navigator.push('/');
  };

  render() {
    const params = {
      title: '403',
      text: 'Доступ запрещен',
      imgSrc: '/images/x403.svg',
      reactNode: (
        <Components.Button
          type="primary"
          onClick={this.onClick}
          fiTitle="Перейти на главную"
        />
      )
    };
    return <Exception {...params} />;
  }
}

export default Decorators.classDecorate(Error403, [
  DNotUsedAreaView,
  errorController.DRegisterError(403),
  DRouteQuery(DPublicRoute())
]);
