import * as React from 'react';
import Decorators from 'berish-decorate';
import Exception from './exception';
import { errorController } from '../../abstract/global';
import { IPageControllerProps } from '../../abstract/global/pageController';
import {
  DRouteQuery,
  DPublicRoute,
  DNotUsedAreaView
} from '../../abstract/util/decorators/route';
import * as Components from '../../abstract/components';

class Error404 extends React.Component<IPageControllerProps> {
  constructor(props) {
    super(props);
  }

  onClick = () => {
    this.props.controller.navigator.push('/');
  };

  render() {
    const params = {
      title: '404',
      text: 'Страницы не существует',
      imgSrc: '/images/x404.svg',
      reactNode: (
        <Components.Button
          type="primary"
          onClick={this.onClick}
          fiTitle="Перейти на главную"
        />
      )
    };
    return <Exception {...params} />;
  }
}
export default Decorators.classDecorate(Error404, [
  DNotUsedAreaView,
  errorController.DRegisterError(404),
  DRouteQuery(DPublicRoute())
]);
