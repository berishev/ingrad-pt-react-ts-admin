import * as React from 'react';

interface IExeptionProps {
  title: string;
  text: string;
  imgSrc: string;
  reactNode?: React.ReactNode;
}
export default class Exception extends React.Component<IExeptionProps, {}> {
  render() {
    let { title, text, imgSrc, reactNode } = this.props;
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100vh'
        }}
      >
        <img src={imgSrc} />
        <div
          style={{
            width: '300px',
            marginLeft: '40px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div
            style={{
              color: '#434e59',
              fontSize: '72px',
              fontWeight: 600,
              lineHeight: '72px',
              marginBottom: '24px'
            }}
          >
            {title}
          </div>
          <div
            style={{
              fontFamily: 'Verdana, Geneva, Tahoma, sans-serif',
              fontSize: '15px',
              padding: '15px 0'
            }}
          >
            {text}
          </div>
          {reactNode}
        </div>
      </div>
    );
  }
}
