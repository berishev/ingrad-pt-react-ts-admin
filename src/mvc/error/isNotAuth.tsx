import * as React from 'react';
import Decorators from 'berish-decorate';
import { errorController } from '../../abstract/global';
import { Redirect } from 'react-router';

class IsNotAuth extends React.Component<any> {
  constructor(props) {
    super(props);
  }

  render() {
    return <Redirect to="/auth/signin" push />;
  }
}
export default Decorators.classDecorate(IsNotAuth, [
  errorController.DRegisterError('isNotAuth')
]);
