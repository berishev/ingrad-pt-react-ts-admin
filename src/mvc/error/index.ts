import Error403 from './error403';
import Error404 from './error404';
import Error500 from './error500';
import IsNotAuth from './isNotAuth';
export default {
  Error403,
  Error404,
  Error500,
  IsNotAuth
};
