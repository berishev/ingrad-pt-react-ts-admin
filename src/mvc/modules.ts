import error from './error';
import home from './home/controller';
import teamUser from './teamUser';
import project from './project';
import team from './team';

export default {
  error,
  home,
  teamUser,
  project,
  team
};
