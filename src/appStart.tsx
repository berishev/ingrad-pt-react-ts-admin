import * as ringle from 'berish-ringle';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './styles';
import App from './mvc';

class AppStart {
  start() {
    ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
  }
}

export default ringle.getSingleton(AppStart);
