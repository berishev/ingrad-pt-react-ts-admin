import * as React from 'react';
import * as Parse from 'parse';
import QueryLINQ from 'berish-parse-query-linq';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { PageController } from '../../abstract/global/pageController';
import { Selectfield } from '../../abstract/components';
import { ISelectFieldProps } from '../../abstract/components/selectfield';
import {
  AbstractComponent,
  IFormItemProps
} from '../../abstract/components/abstract';
import executeController from '../../abstract/global/executeController';

interface IProps
  extends ISelectFieldProps<Model.SPOrgStructure>,
    IFormItemProps {}

interface IState {
  data: LINQ<Model.SPOrgStructure>;
}

export default class extends AbstractComponent<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      data: LINQ.fromArray([])
    };
  }

  componentDidMount() {
    executeController.tryLoad(this.onLoad);
  }

  onLoad = async (nextProps?: IProps) => {
    let query = Model.SPOrgStructure.getQuery();
    let data = await QueryLINQ(query);
    this.setState({
      data
    });
  };

  renderData = () => {
    let { data } = this.state;
    return data.orderBy(m => m.spID).select(m => {
      return {
        value: m,
        view: `${m.spID} - ${m.LinkTitle}`
      };
    });
  };

  render() {
    return (
      <Selectfield
        placeholder="Выберите SP-Оргструтуру"
        data={this.renderData()}
        {...this.props}
      />
    );
  }
}
