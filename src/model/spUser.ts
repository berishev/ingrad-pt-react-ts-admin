import * as Parse from 'parse';
import Enum from 'berish-enum';
import * as Model from './';
import { EnumValueOf } from 'berish-enum/dist/lib';

export const SPUserSexEnum = Enum.createFromPrimitive('male', 'female');
export type SPUserSexEnumType = EnumValueOf<typeof SPUserSexEnum>;

export class SPUser extends Parse.Object {
  static getQuery(query?: Parse.Query<SPUser>) {
    query = query || new Parse.Query(SPUser);
    query = query.include(['department']).ascending('LinkTitle');
    return query;
  }

  constructor() {
    super('SPUser');
  }

  get spID() {
    return this.get('spID');
  }

  set spID(value: number) {
    this.set('spID', value);
  }

  get metadata() {
    return this.get('metadata');
  }

  set metadata(value: any) {
    this.set('metadata', value);
  }

  get login() {
    return this.get('login');
  }

  set login(value: string) {
    this.set('login', value);
  }

  get firstname() {
    return this.get('firstname');
  }

  set firstname(value: string) {
    this.set('firstname', value);
  }

  get lastname() {
    return this.get('lastname');
  }

  set lastname(value: string) {
    this.set('lastname', value);
  }

  get patronymic() {
    return this.get('patronymic');
  }

  set patronymic(value: string) {
    this.set('patronymic', value);
  }

  get photo() {
    return this.get('photo');
  }

  set photo(value: string) {
    this.set('photo', value);
  }

  get position() {
    return this.get('position');
  }

  set position(value: string) {
    this.set('position', value);
  }

  get phoneWork() {
    return this.get('phoneWork');
  }

  set phoneWork(value: string) {
    this.set('phoneWork', value);
  }

  get personalPhone() {
    return this.get('personalPhone');
  }

  set personalPhone(value: string) {
    this.set('personalPhone', value);
  }

  get email() {
    return this.get('email');
  }

  set email(value: string) {
    this.set('email', value);
  }

  get idonec() {
    return this.get('idonec');
  }

  set idonec(value: string) {
    this.set('idonec', value);
  }

  get snils() {
    return this.get('snils');
  }

  set snils(value: string) {
    this.set('snils', value);
  }

  get inn() {
    return this.get('inn');
  }

  set inn(value: string) {
    this.set('inn', value);
  }

  get organization() {
    return this.get('organization');
  }

  set organization(value: string) {
    this.set('organization', value);
  }

  get currentEmployee() {
    return this.get('currentEmployee');
  }

  set currentEmployee(value: string) {
    this.set('currentEmployee', value);
  }

  get interests() {
    return this.get('interests');
  }

  set interests(value: string) {
    this.set('interests', value);
  }

  get sphere() {
    return this.get('sphere');
  }

  set sphere(value: string) {
    this.set('sphere', value);
  }

  get office() {
    return this.get('office');
  }

  set office(value: string) {
    this.set('office', value);
  }

  get sex() {
    return SPUserSexEnum[this.get('sex') as string];
  }

  set sex(value: SPUserSexEnumType) {
    this.set('sex', SPUserSexEnum[value]);
  }

  get personalEmail() {
    return this.get('personalEmail');
  }

  set personalEmail(value: string) {
    this.set('personalEmail', value);
  }

  get otherContact() {
    return this.get('otherContact');
  }

  set otherContact(value: string) {
    this.set('otherContact', value);
  }

  get media() {
    return this.get('media');
  }

  set media(value: string) {
    this.set('media', value);
  }

  get articles() {
    return this.get('articles');
  }

  set articles(value: string) {
    this.set('articles', value);
  }

  get skills() {
    return this.get('skills');
  }

  set skills(value: string) {
    this.set('skills', value);
  }

  get phoneWorker() {
    return this.get('phoneWorker');
  }

  set phoneWorker(value: string) {
    this.set('phoneWorker', value);
  }

  get phoneAdditional() {
    return this.get('phoneAdditional');
  }

  set phoneAdditional(value: string) {
    this.set('phoneAdditional', value);
  }

  get department() {
    return this.get('department');
  }

  set department(value: Model.SPOrgStructure) {
    this.set('department', value);
  }

  get isArchive() {
    return this.get('isArchive');
  }

  set isArchive(value: boolean) {
    this.set('isArchive', value);
  }

  get hireDate() {
    return this.get('hireDate');
  }

  set hireDate(value: Date) {
    this.set('hireDate', value);
  }

  get birthday() {
    return this.get('birthday');
  }

  set birthday(value: Date) {
    this.set('birthday', value);
  }

  get hidetel() {
    return this.get('hidetel');
  }

  set hidetel(value: boolean) {
    this.set('hidetel', value);
  }

  get displayOnPortal() {
    return this.get('displayOnPortal');
  }

  set displayOnPortal(value: boolean) {
    this.set('displayOnPortal', value);
  }

  get floorNumber() {
    return this.get('floorNumber');
  }

  set floorNumber(value: number) {
    this.set('floorNumber', value);
  }

  get LinkTitle() {
    return this.get('LinkTitle');
  }

  set LinkTitle(value: string) {
    this.set('LinkTitle', value);
  }
}

Parse.Object.registerSubclass('SPUser', SPUser);
