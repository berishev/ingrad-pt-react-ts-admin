import * as Parse from 'parse';
import Decorators from 'berish-decorate';
import { IsRequired } from '../abstract/util/decorators/filter';

import * as Model from './';

export class Team extends Parse.Object {
  static getQuery(query?: Parse.Query<Team>) {
    query = query || new Parse.Query(Team);
    query = query.include(['spOrg']).ascending('name');
    return query;
  }

  constructor() {
    super('Team');
  }

  get name() {
    return this.get('name');
  }

  set name(value: string) {
    this.set('name', value);
  }

  get fullname() {
    return this.get('fullname');
  }

  set fullname(value: string) {
    this.set('fullname', value);
  }

  get spOrg() {
    return this.get('spOrg');
  }

  set spOrg(value: Model.SPOrgStructure) {
    this.set('spOrg', value);
  }

  get positions() {
    let value = this.get('positions');
    if (!value) this.set('positions', []);
    return this.get('positions');
  }

  set positions(value: string[]) {
    this.set('positions', value);
  }
}

Decorators.methodDecorate(Team, 'name', [IsRequired()]);

Parse.Object.registerSubclass('Team', Team);
