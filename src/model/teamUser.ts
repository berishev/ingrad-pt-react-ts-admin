import * as Parse from 'parse';
import Decorators from 'berish-decorate';
import * as Model from './';
import {
  IsRequired,
  IsPhone,
  Custom
} from '../abstract/util/decorators/filter';

export class TeamUser extends Parse.Object {
  static getQuery(query?: Parse.Query<Model.TeamUser>) {
    query = query || new Parse.Query(Model.TeamUser);
    query = query.include(['team', 'projects', 'spUser']);
    return query;
  }

  constructor() {
    super('TeamUser');
  }

  get team() {
    return this.get('team');
  }

  set team(value: Model.Team) {
    this.set('team', value);
  }

  get position() {
    return this.get('position');
  }

  set position(value: string) {
    this.set('position', value);
  }

  get task() {
    return this.get('task');
  }

  set task(value: string) {
    this.set('task', value);
  }

  get lastname() {
    return this.get('lastname');
  }

  set lastname(value: string) {
    this.set('lastname', value);
  }

  get name() {
    return this.get('name');
  }

  set name(value: string) {
    this.set('name', value);
  }

  get patronymic() {
    return this.get('patronymic');
  }

  set patronymic(value: string) {
    this.set('patronymic', value);
  }

  get phoneCode() {
    return this.get('phoneCode');
  }

  set phoneCode(value: number) {
    this.set('phoneCode', value);
  }

  get phoneMobile() {
    return this.get('phoneMobile');
  }

  set phoneMobile(value: string) {
    this.set('phoneMobile', value);
  }

  get officeNumber() {
    return this.get('officeNumber');
  }

  set officeNumber(value: string) {
    this.set('officeNumber', value);
  }

  get floorOrRoom() {
    return this.get('floorOrRoom');
  }

  set floorOrRoom(value: string) {
    this.set('floorOrRoom', value);
  }

  get projects() {
    let value = this.get('projects');
    if (!value) this.set('projects', []);
    return this.get('projects');
  }

  set projects(value: Model.Project[]) {
    this.set('projects', value);
  }

  get spUser() {
    return this.get('spUser');
  }

  set spUser(value: Model.SPUser) {
    this.set('spUser', value);
  }
}

Decorators.methodDecorate(TeamUser, 'team', [IsRequired()]);
Decorators.methodDecorate(TeamUser, 'position', [IsRequired()]);
Decorators.methodDecorate(TeamUser, 'lastname', [IsRequired()]);
Decorators.methodDecorate(TeamUser, 'phoneMobile', [IsPhone()]);
Decorators.methodDecorate(TeamUser, 'projects', [
  Custom(
    (value: Model.Project[]) => value.length > 0,
    'Проектов должно быть больше 0'
  )
]);

Parse.Object.registerSubclass('TeamUser', TeamUser);
